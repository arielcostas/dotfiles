#!/bin/sh
if ! { command -v curl; } > /dev/null; then
	echo "Curl not installed"
	exit 1C
fi

curl -s "https://min-api.cryptocompare.com/data/price?fsym=XMR&tsyms=USD" | sed 's/[^0-9.]//g'
