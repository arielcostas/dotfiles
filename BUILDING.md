# PHP

```sh
./configure --with-password-argon2 --with-pdo-mysql --with-pdo-pgsql --enable-mbstring --with-openssl
make -j 6
make install
```

# Vim

```sh
./configure --with-gnome --with-x --enable-terminal
make -j 6
make install
```
