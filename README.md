# Ariel Costas' Dotfiles Repository

Welcome to my dotfiles, personalized configuration files for my computer
programs. Handling dotfiles is really easy thanks to [GNU
Stow](https://www.gnu.org/software/stow/) and Git.

## Stow and Git

In the past I managed my dotfiles manually with a "config" command alias that
called git with home as my working tree and a repository somewhere else as my
git-dir. This was not terrible, but I had to manually add configuration files
and make sure to not add anything I didn't want. Additionally, files like the
README had to be in my home directory, which didn't make sense.

On september 29, 2021 I migrated this over to a Stow-based system, where I have
a directory for each program and inside the folder structure relative to my
home. When I run `stow [folder]`, that folder gets linked to the directory above
it. 

Say I want to _install_ my bash config, I would clone the repo inside my home
directory `/home/ariel/dotfiles` and run `stow bash` from the inside. Next time
I loaded bash, my bash profile would be loaded as if it was there, but its only
a link to the file inside my `dotfiles folder`.

I then add all the files to git inside that repo and commit, without having my
home directory cluttered with README.md, gitignore and all that crap.

# What this repo contains

- alacritty: The terminal emulator I used in the past, now superseded by suckless st all my software projects
- bash: Shell
- git: The source control software this repo is built on, and I use to manage
- picom: X Window Compositor (using picom-jonaburg)
- startX: the init file for graphical environment
- i3-blocks: Status bar generator for the i3 window manager
- i3-gaps: X11 Tiling Window Manager
- vim: Terminal-based text editor I use for scripts and config files

## Old packages

Might not be updated or work in the latest version, kept for historical
purposes. Use at your own risk.

- qtile: Tiling window manager for the X Window System
- sway: Wayland compositor, drop-in replacement of i3
- waybar: bar implementation for Sway

## Extra packages

I use these packages too, but with the default config

- exa
- rofi
- yt-dlp
- fzf for bash history
- perl-rename (optional, recommended)

# Custom compilation options

There's a file on this repo called BUILDING.md, which is a (small) list of
configurations I use for compiling certain programs.
