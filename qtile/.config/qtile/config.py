from typing import List  # noqa: F401

from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile.widget import base

import os
import subprocess

mod = "mod4"
terminal = guess_terminal()
terminal = "alacritty"

colors = {
    'innest': 'EF4444',
    'inner': 'DC2626',
    'middle' :'B91C1C',
    'outer': '991B1B',
}

keys = [
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "space", lazy.layout.next()),

    Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),

    Key([mod, "control"], "h", lazy.layout.grow(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.shrink(),
        desc="Grow window to the right"),
    Key([mod, "control"], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawn("dmenu_run"),
        desc="Spawn a command using a prompt widget"),

    Key([mod], "f", lazy.spawn("firefox"), desc="Launch web browser"),
    Key([mod], "e", lazy.spawn("nemo"), desc="Launch file explorer"),
    Key([mod], "t", lazy.spawn("emacsclient -c --eval '(rmail)'"), desc="Launch mail client"),
    Key([mod], "m", lazy.spawn("emacsclient -c"), desc="Launch text editor"),
    Key([mod, "shift"], "s", lazy.spawn("ksnip"), desc="Launch screenshot tool"),
]

groups = [Group(i) for i in [
    " 1 ", " 2 ", " 3 ", " 4 ", " 5 "
]]
for i, group in enumerate(groups):
    actual_key = str(i + 1)
    keys.extend([
        Key([mod], actual_key, lazy.group[group.name].toscreen()),

        Key([mod, "shift"], actual_key, lazy.window.togroup(group.name, switch_group=True)),
    ])

layouts = [
    layout.MonadTall(border_width=2,margin=6,ratio=0.65,border_focus=colors['middle']),
    layout.Max(),
    layout.MonadTall(border_width=2,margin=6,ratio=0.5,border_focus=colors['middle']),
]

widget_defaults = dict(
    font='LiterationMono Nerd Font',
    fontsize=17,
    padding=3
)
extension_defaults = widget_defaults.copy()

bar_margin = [5, 9, 0, 9]
top_bar = bar.Bar(
            [
                widget.GroupBox(
                    padding_x=8,
                    margin_x=0,
					margin_y=4,
                    rounded = False,
					disable_drag = True,
					highlight_color=['000000', '000000'],
                    borderwidth = 3,
                    highlight_method = 'line',
                    this_current_screen_border = colors['inner'],
                    this_screen_border = colors['inner'],
					inactive="808080"
                ),
                widget.Prompt(),
                widget.WindowName(max_chars=80),
                widget.TextBox(text="  ", background=colors['innest'],padding=5),
                widget.CheckUpdates(custom_command="pacman -Q", background=colors['innest'], display_format="{updates} "),
                widget.OpenWeather(cityid=3105976,background=colors['inner'],foreground="ffffff",margin=4,format=" 摒 {main_temp}º - {weather}"),
                widget.Spacer(length=5,background=colors['inner']),
                widget.CurrentLayoutIcon(scale=0.5,background=colors['middle']),
                widget.CurrentLayout(background=colors['middle']),
                widget.TextBox(text=" ", background=colors['outer'],padding=5),
                widget.Clock(format='%B %d - %H:%M',background=colors['outer']),
            ],
            28,
		margin=bar_margin,
		opacity=0.8
)

screens = [
    Screen(top=top_bar),
		Screen()
]

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Click([mod], "Button2", lazy.window.toggle_floating()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
]

dgroups_key_binder = None
dgroups_app_rules = []
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    *layout.Floating.default_float_rules,
    Match(title='pinentry'),  # gnupg
    Match(title='Qalculate!'),  # qalculate-gtk
    Match(title='ksnip'),  # ksnip
])
auto_fullscreen = True
focus_on_window_activation = "smart"

wmname = "LG3D"

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])

@hook.subscribe.layout_change
def on_layout_change():
    if bar_margin == [0,0,0,0]:
        bar_margin = [5,9,0,9]
    else:
        bar_margin = [0,0,0,0]
    top_bar.draw()
