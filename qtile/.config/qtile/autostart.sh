#!/bin/sh
feh --bg-scale /home/ariel/wallpaper.jpg
picom --experimental-backends &
easyeffects --gapplication-service &
emacs --daemon &
