#!/bin/sh
if ! { command -v curl; } > /dev/null; then
	echo "Curl not installed"
	exit 1C
fi

if ! { command -v jq; } > /dev/null; then
	echo "jq not installed"
	exit 1C
fi

curl -s "https://servizos.meteogalicia.gal/rss/observacion/estadoEstacionsMeteo.action?idEst=10142" | jq .listEstadoActual[0].valorTemperatura
